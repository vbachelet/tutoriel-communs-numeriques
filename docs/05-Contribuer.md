# Contribuez !


## Contributeurs actuels

Retrouvez la liste des contributeur.ices actuel.les du projet  [ici](https://vbachelet.frama.io/tutoriel-communs-numeriques/contributors/).

## Pour contribuer

Toutes les ressources du projet sont disponibles sur la [plateforme Framagit](https://framagit.org/inno3/tutoriel-communs-numeriques). 
Si vous ne connaissez pas GitLab, nous vous invitons à consulter la [documentation proposée par Framasoft](https://docs.framasoft.org/fr/gitlab/).

Un atelier autour du Tutoriel, à destination de tous les intéréssés, nouveaux ou anciens , et qui se tiendra dans les locaux d'Inno3, est également en cours de préparation. Vous pouvez indiquer la date qui vous conviendrait le mieux sur ce [framadate](https://framadate.org/6IMi8lq4EmTSZAbi)].

Par ailleurs, n'hésitez pas à nous contacter pour venir contribuer au projet (nec@inno3.fr) et/ou à vous abonner à la [liste de diffusion du projet](https://framalistes.org/sympa/info/tutoriel-communs-numeriques).
