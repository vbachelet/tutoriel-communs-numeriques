#Introduction à l'atelier sur la production de ressources

##Présentation de la démarche ayant mené à la création du logiciel Zourit -- Pascal Gascoin 

###Historique 
Les CEMEA sont une association d'éducation populaire, le développement de logiciel n'est absolument pas notre spécialité.

Cela fait plus de 20 ans que l'on essaie, caricaturalement, de faire le lien entre geeks asociaux et utilisateurs basiques.

Le lien entre l'éduc pop et le libre est si évident que personne n'en avait conscience. Avec les CEMEA, on a commencé à faire des ateliers autour des logiciels libres, en les proposant aux collectivités, elles y voyaient un intérêt, mais la difficulté survenait avec la partie installation.

On a donc suivi le mouvement Degooglisons de Framasoft.

Tout un travail a été mené sur la conformité de nos usages, notamment la protection des données; d'autant plus que les CEMEA sont un réseau en étoile sans standard, certains développaient des solutions en interne non documentées, impossible à reprendre par les autres, etc.

La décision a donc été prise, pour les CEMEA d'abord, puis pour l'extérieur, de se doter d'un outil permettant de remplir les taches quotidienne, protecteur des données (auto-hébergé). N'ayant pas trouvé satisfaction avec l'existant, on a décidé de construire l'outil nous-meme, avec la contribution des futurs utilisateurs finaux, en se posant la question des avantages et inconvénients/manques des outils que l'on utilisait; ce qui a donné naissance à un cahier des charges copieux. On a repris les avantages de tous les outils déjà existants, et on a développé la tête, qui permet d'avoir accès à tous les services d'un coup (zourit = la pieuvre en créole).

###Craintes 
On va mettre de l'argent dans le développement d'un outil que l'on va ensuite rendre libre ; mais le coût d'une licence propriétaire n'est pas loin d'être le même avec des bénéfices moindres.

###Avantages 
Zourit est en perpétuel développement, il n'a pas encore été récupéré car il est lourd à faire tourner, mais il intéresse énormément d'associations. Maintenant, on se retrouve à héberger pour d'autres petites associations, que l'on forme en parallèle. *

###Questions 
**Combien de temps la mise en place a t-elle pris ?** 3 ans entre les premières discussions et la première bascule vers Zourit Passage à l'échelle : de 0 à 24 associations, pour près de 1000 utilisateurs + ouverture prochaine d'un serveur pour 50 associations.