![NEC](images/LogoNEC_siteweb.png) 

**Welcome to the project site "Digital Commons Tutorial". This is an initiative driven by the Digital [Society mission](https://societenumerique.gouv.fr/) (of the Digital Agency) and the [MedNum](https://lamednum.coop/) and coordinated by [Inno³](https://inno3.fr).**

**The current version of the tutorial available on this site is the result of collaborative work several times, mixing digital and physical contributions.**

##A digital common?

A common is a resource produced and/or maintained collectively by a community of heterogeneous actors, and governed by democratically established rules that ensure its collective and shared character.

It is called digital when the resource is digital: software, database, digital content (text, image, video and/or sound), etc. It is thus possible to speak of digital commons in the presence of co-produced and collectively governed Open Data databases, Free/Open Source Software or open content meeting the same rules.

##Why this tutorial?

**To respond to a growing need for actors, public as well as private, who wish to initiate approaches to digital commons** (either to open resources that they develop or maintain, or to contribute to pre-existing or to-build third-partie-resources, etc.) and who seek an accompaniment.

It is therefore a question of providing concrete answers to their questions, of gradually lifting the cultural, technical, organizational and economic barriers often associated with opening up and pooling approaches.

This work takes the form of a **generic** tutorial, **instantiable** for specific needs, to which are associated various **reference systems** in support of this harmonization and extension approach.

First instantiations: 
* By the [Mission Digital Society](https://societenumerique.github.io/tutoriel), for local authorities 
* By the association Open Law, Open Law: in progress

##A tutorial for whom?

This tutorial is for **all those who wish to develop a project of digital commons, as well as all people - public and private - involved in the maintenance or development of such projects** .

##Our approach

###Digital in Common[s]

This initiative was initiated on the occasion of [Numérique en Commun](http://numerique-en-commun.fr/) in Nantes last September, as part of the course "Digital Commons" composed of 5 workshops spread over the 2 days of the event.

These workshops gave rise to reports available on the [NEC website](https://pad.numerique-en-commun.fr/parcours-communs#) as well as a series of insider references, in support of the tutorial and in the same logic of constitution of commons, in the idea of concretely accompanying the implementation. implementation of the principles contained in the tutorial.

###Workshops

The reflection then continued during a [meet-up organized at the Maison du Libre et des Communs last October](https://inno3.fr/evenements/meetup-contributeurs-du-parcours-communs-numeriques), the opportunity for a time of exchange to draw up an initial list of issues around which to structure the final tutorial.

As an extension of this approach, an e[(xchange evening was organized in Bercy at the end of 2018](https://inno3.fr/evenements/soiree-de-presentation-du-tutoriel-communs-numeriques-le-1912-18h) to present the first version of the tutorial for the digital commons approaches (questions, answers, resources and examples) and to prefigure the next steps of this collective course.

###The following

This initial work is now intended to be perfected and/or supplemented by other resources and/or contributors, in a longer-term, open and collaborative approach to build a solid state of the art.

In parallel, the tutorial thus made available is intended to be understood by any type of project, organization or actor wishing to support a digital commons approach. For that, do not hesitate to implement it directly by adapting it to your needs and specificities.

##Contributors

###Current contributors

Find the list of current project contributors [here](https://vbachelet.frama.io/tutoriel-communs-numeriques/contributors/)

###To contribute

All project resources are available on the [Framagit platform](https://framagit.org/inno3/tutoriel-communs-numeriques) .

In addition, do not hesitate to contact us to come to contribute to the project (nec@inno3.fr) and/or to subscribe to the [project mailing list](https://framalistes.org/sympa/info/tutoriel-communs-numeriques). 